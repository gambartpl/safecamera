Things to be done in next steps:

1. Observe if encrypted file has been deleted manually (out of the application) from the file system and notify UI appropriately
2. Use 3rd party lib for permission handling (e.g. PermissionDispatcher) to avoid callback hell ;)
3. Display bitmap in dialog when clicked in RecyclerView.
4. Display notification whether photo is encrypted or not (e.g. badge in photo top right corner).
5. Do tests for possible edge cases.