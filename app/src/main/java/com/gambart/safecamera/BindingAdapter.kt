package com.gambart.safecamera

import android.widget.ImageView
import androidx.databinding.BindingAdapter
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions
import com.gambart.safecamera.model.Photo

@BindingAdapter("photo")
fun ImageView.photo(photo: Photo) {
    if (photo.decoded.not()) {
        Glide.with(this.context)
            .load(R.drawable.ic_baseline_photo_24)
            .into(this)
    } else {
        Glide.with(this.context)
            .load(photo.imageByteArray)
            .transition(DrawableTransitionOptions.withCrossFade())
            .into(this)
    }
}