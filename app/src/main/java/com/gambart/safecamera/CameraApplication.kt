package com.gambart.safecamera

import android.app.Application
import android.util.Log
import com.gambart.safecamera.di.commonModule
import com.gambart.safecamera.di.repositoryModule
import com.gambart.safecamera.di.storageModule
import com.gambart.safecamera.di.viewModelModule
import org.koin.android.ext.koin.androidContext
import org.koin.android.ext.koin.androidLogger
import org.koin.core.context.startKoin
import timber.log.Timber

class CameraApplication : Application() {

    override fun onCreate() {
        super.onCreate()
        setupKoin()
        setupTimber()
    }

    private fun setupKoin() {
        startKoin {
            androidLogger()
            androidContext(this@CameraApplication)
            modules(viewModelModule, repositoryModule, storageModule, commonModule)
        }
    }

    private fun setupTimber() {
        if (BuildConfig.DEBUG) Timber.plant(Timber.DebugTree())
        else Timber.plant(ReleaseTree())
    }

    private class ReleaseTree : Timber.Tree() {

        override fun isLoggable(tag: String?, priority: Int): Boolean {
            if (priority == Log.VERBOSE || priority == Log.DEBUG || priority == Log.INFO) {
                return false
            }
            return true
        }

        override fun log(priority: Int, tag: String?, message: String, t: Throwable?) {
            if (!isLoggable(tag, priority)) {
                return
            }

            if (priority == Log.WARN) {
                Log.w(tag, message)
            } else if (priority == Log.ERROR) {
                Log.e(tag, message)
            }
        }
    }
}