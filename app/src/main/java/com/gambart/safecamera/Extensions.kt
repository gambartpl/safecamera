package com.gambart.safecamera

fun <T> Iterable<T>.copy(): List<T> = mutableListOf<T>().apply { addAll(this) }

fun <T> Iterable<T>.copyAndInsert(item: T): List<T> = mutableListOf<T>().apply {
    addAll(this@copyAndInsert)
    add(item)
}

fun <T> List<T>.replaceIf(item: T, expression: (T) -> Boolean) =
    find(expression)?.let { replace(it, item) } ?: this

fun <T> Iterable<T>.replace(old: T, new: T) = map { if (it == old) new else it }

fun <T> Iterable<T>.replaceAt(index: Int, elem: T) = mapIndexed { i, existing -> if (i == index) elem else existing }