package com.gambart.safecamera.di

import android.content.SharedPreferences
import androidx.security.crypto.EncryptedSharedPreferences
import androidx.security.crypto.MasterKeys
import com.gambart.safecamera.processor.DecryptProcessor
import com.gambart.safecamera.processor.EncryptProcessor
import com.gambart.safecamera.repository.PhotoRepository
import com.gambart.safecamera.repository.UserRepository
import com.gambart.safecamera.security.Encrypt
import com.gambart.safecamera.security.SecurityEncrypt
import com.gambart.safecamera.storage.Preferences
import com.gambart.safecamera.ui.gallery.GalleryViewModel
import com.gambart.safecamera.ui.login.LoginViewModel
import org.koin.android.ext.koin.androidContext
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

const val SHARED_PREFS_FILE_NAME = "camera_prefs"

val commonModule = module {
    single<Encrypt> { SecurityEncrypt(androidContext()) }
    single { EncryptProcessor(get(), androidContext()) }
    single { DecryptProcessor(get()) }
}

val viewModelModule = module {
    viewModel { LoginViewModel(get()) }
    viewModel { GalleryViewModel(get()) }
}

val repositoryModule = module {
    single { UserRepository(get()) }
    single { PhotoRepository(androidContext(), get(), get()) }
}

val storageModule = module {

    single<SharedPreferences> {
        EncryptedSharedPreferences.create(
            SHARED_PREFS_FILE_NAME,
            MasterKeys.getOrCreate(MasterKeys.AES256_GCM_SPEC),
            androidContext(),
            EncryptedSharedPreferences.PrefKeyEncryptionScheme.AES256_SIV,
            EncryptedSharedPreferences.PrefValueEncryptionScheme.AES256_GCM)
    }

    single { Preferences(get()) }
}

