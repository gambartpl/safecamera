package com.gambart.safecamera.model

import com.gambart.safecamera.utils.fileDateFormat
import com.gambart.safecamera.utils.shortDateFormat
import java.io.File

/**
 * Helper class containing `Photo` instance and temporary unencrypted file image.
 */
data class PhotoWithFile(
    val photo: Photo,
    val tempFile: File
)

/**
 * Photo can exist in two variants: decoded and not decoded.
 * When photo is instantiated, it's assigned only `fileName` and `decoded` returns false.
 * Once decryption is finished `imageByteArray` is filled with image bytes and `decoded` returns true.
 *
 * Due to `ByteArray` property, `hashCode` and `equals` have been overridden.
 * Both `fileName` and `decrypted` fields are used because of `StateFlow`'s `Any.equals` contract:
 * Decoded and not-decoded photos MUST NOT be equal in order to properly refresh
 * RecyclerView adapter (DiffUtil).
 */
data class Photo(val fileName: String, val imageByteArray: ByteArray = ByteArray(0)) {

    val date: String
        get() {
            val date = fileDateFormat.parse(fileName)
            return shortDateFormat.format(date)
        }

    val decoded: Boolean
        get() = imageByteArray.isNotEmpty()

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as Photo

        if (fileName != other.fileName) return false
        if (decoded != other.decoded) return false

        return true
    }

    override fun hashCode(): Int {
        var result = fileName.hashCode()
        result = 31 * result + decoded.hashCode()
        return result
    }
}