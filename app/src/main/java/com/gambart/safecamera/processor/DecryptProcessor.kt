package com.gambart.safecamera.processor

import com.gambart.safecamera.model.Photo
import com.gambart.safecamera.model.PhotoWithFile
import com.gambart.safecamera.security.Encrypt
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.channels.BroadcastChannel
import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.asFlow
import kotlinx.coroutines.flow.flowOn
import kotlinx.coroutines.flow.map

@OptIn(
    kotlinx.coroutines.ExperimentalCoroutinesApi::class,
    kotlinx.coroutines.FlowPreview::class
)
class DecryptProcessor(private val encrypt: Encrypt) {

    private val photosChannel = BroadcastChannel<PhotoWithFile>(Channel.BUFFERED)

    val processedPhotos: Flow<Photo> = photosChannel.asFlow()
        .map { (photo, file) ->
            val bytes = encrypt.decryptAsByteArray(file)
            photo.copy(imageByteArray = bytes)
        }
        .flowOn(Dispatchers.Default)

    fun decode(photoWithFile: PhotoWithFile) {
        photosChannel.offer(photoWithFile)
    }
}