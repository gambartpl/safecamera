package com.gambart.safecamera.processor

import android.content.Context
import com.gambart.safecamera.model.Photo
import com.gambart.safecamera.security.Encrypt
import com.gambart.safecamera.utils.FileHelper
import kotlinx.coroutines.channels.BroadcastChannel
import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.asFlow
import kotlinx.coroutines.flow.onEach

@OptIn(
    kotlinx.coroutines.ExperimentalCoroutinesApi::class,
    kotlinx.coroutines.FlowPreview::class
)
class EncryptProcessor(private val encrypt: Encrypt, private val context: Context) {

    private val photosChannel = BroadcastChannel<Photo>(Channel.BUFFERED)

    val processedPhotos: Flow<Photo> = photosChannel.asFlow()
        .onEach { photo ->
            val encryptedFile = FileHelper.createImageFile(photo.fileName, context)
            encrypt.encryptByteArray(photo.imageByteArray, encryptedFile)
        }

    fun encode(photo: Photo) {
        photosChannel.offer(photo)
    }
}