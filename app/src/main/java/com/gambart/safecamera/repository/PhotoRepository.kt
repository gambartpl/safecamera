package com.gambart.safecamera.repository

import android.content.Context
import com.gambart.safecamera.model.Photo
import com.gambart.safecamera.model.PhotoWithFile
import com.gambart.safecamera.processor.DecryptProcessor
import com.gambart.safecamera.processor.EncryptProcessor
import com.gambart.safecamera.utils.FileHelper
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.merge
import kotlinx.coroutines.withContext

class PhotoRepository(
    private val context: Context,
    private val decryptProcessor: DecryptProcessor,
    private val encryptProcessor: EncryptProcessor
) {

    /**
     * Flow with photos that have been either decrypted or encrypted.
     *
     * Note: At this moment UI does not show any notification the photo has been successfully
     * encrypted and this emission of encrypted photos could be skipped. However, in case we want to
     * improve UI, the logic is prepared
     */
    @OptIn(kotlinx.coroutines.ExperimentalCoroutinesApi::class)
    val processedPhotosFlow = merge(
        decryptProcessor.processedPhotos,
        encryptProcessor.processedPhotos
    )

    /**
     * 1. Create decoded photo
     * 2. Return it and enqueue for encryption
     */
    fun createPhoto(imageByteArray: ByteArray): Photo {
        val photo = Photo(FileHelper.getEncryptedFileName(), imageByteArray)
        encryptProcessor.encode(photo)
        return photo
    }

    /**
     * 1. Create not decoded photos from encrypted files.
     * 2. Return it and enqueue for decryption
     */
    suspend fun getPhotos(): List<Photo> = withContext(Dispatchers.Default) {
        FileHelper.getFiles(context)
            .map { encryptedFile ->
                PhotoWithFile(Photo(encryptedFile.nameWithoutExtension), encryptedFile) }
            .sortedBy { it.photo.fileName }
            .onEach { decryptProcessor.decode(it) }
            .map { (photo, _) -> photo }
    }
}