package com.gambart.safecamera.repository

import com.gambart.safecamera.storage.Preferences

/**
 * Sets default password on first run.
 */
class UserRepository(private val preferences: Preferences) {

    init {
        if (preferences.getPassword() == null)
            preferences.setPassword("pass")
    }

    fun isPasswordValid(password: String) = password == preferences.getPassword()
}