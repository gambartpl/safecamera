package com.gambart.safecamera.security

import java.io.File

interface Encrypt {
    fun encryptByteArray(byteArray: ByteArray, file: File)
    fun decryptAsByteArray(file: File): ByteArray
}