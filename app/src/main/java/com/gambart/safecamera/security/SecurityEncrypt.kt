package com.gambart.safecamera.security

import android.content.Context
import androidx.security.crypto.EncryptedFile
import androidx.security.crypto.MasterKeys
import com.gambart.safecamera.di.SHARED_PREFS_FILE_NAME
import com.gambart.safecamera.storage.KEY_PASSWORD
import java.io.File

class SecurityEncrypt(private val context: Context) : Encrypt {

    override fun encryptByteArray(byteArray: ByteArray, file: File) {
        file.encrypted(context).openFileOutput().use {
            it.write(byteArray)
        }
    }

    override fun decryptAsByteArray(file: File): ByteArray {
        file.encrypted(context).openFileInput().use { inputStream ->
            return inputStream.readBytes()
        }
    }

    fun encrypt(srcFile: File, destFile: File) {
        srcFile.copyTo(destFile.encrypted(context))
    }
}

private fun File.encrypted(context: Context): EncryptedFile =
    EncryptedFile.Builder(
        this,
        context,
        MasterKeys.getOrCreate(MasterKeys.AES256_GCM_SPEC),
        EncryptedFile.FileEncryptionScheme.AES256_GCM_HKDF_4KB
    )
        .setKeysetAlias(KEY_PASSWORD)
        .setKeysetPrefName(SHARED_PREFS_FILE_NAME)
        .build()

private fun File.copyTo(file: EncryptedFile) {
    inputStream().use { input ->
        file.openFileOutput().use { output ->
            input.copyTo(output)
        }
    }
}