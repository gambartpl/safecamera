package com.gambart.safecamera.storage

import android.content.SharedPreferences
import androidx.core.content.edit

const val KEY_PASSWORD = "camera.password"

class Preferences(private val sharedPreferences: SharedPreferences) {

    fun setPassword(password: String) = sharedPreferences.edit {
        putString(KEY_PASSWORD, password)
    }

    fun getPassword(): String? = sharedPreferences.getString(KEY_PASSWORD, null)
}