package com.gambart.safecamera.ui.gallery

import android.Manifest
import android.app.Activity
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.provider.MediaStore
import android.view.View
import androidx.activity.result.contract.ActivityResultContract
import androidx.activity.result.contract.ActivityResultContracts
import androidx.annotation.StringRes
import androidx.core.content.FileProvider
import androidx.fragment.app.Fragment
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.GridLayoutManager
import com.gambart.safecamera.BuildConfig
import com.gambart.safecamera.R
import com.gambart.safecamera.databinding.FragmentGalleryBinding
import com.gambart.safecamera.utils.FileHelper
import com.gambart.safecamera.utils.autoCleared
import com.google.android.material.snackbar.Snackbar
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach
import org.koin.androidx.viewmodel.ext.android.viewModel
import java.io.File
import java.io.IOException

/**
 * `ActivityResultContracts.TakePhoto` returns Boolean denoting if photo has been taken successfully.
 * This class returns File instance that is later used to encrypt the image.
 */
class TakePicture : ActivityResultContract<File, ByteArray?>() {

    private lateinit var file: File

    override fun createIntent(context: Context, input: File): Intent {
        file = input
        val uri = FileProvider.getUriForFile(context, BuildConfig.APPLICATION_ID + ".fileprovider", input)
        return Intent(MediaStore.ACTION_IMAGE_CAPTURE).apply {
            putExtra(MediaStore.EXTRA_OUTPUT, uri)
        }
    }

    override fun parseResult(resultCode: Int, intent: Intent?): ByteArray? {
        val imageByteArray = if (resultCode == Activity.RESULT_OK) file.readBytes() else null
        file.delete()
        return imageByteArray
    }
}

class GalleryFragment : Fragment(R.layout.fragment_gallery) {

    private val viewModel by viewModel<GalleryViewModel>()
    private var binding by autoCleared<FragmentGalleryBinding>()
    private val photoAdapter = PhotoAdapter()

    private val actionCameraPermission =
        registerForActivityResult(ActivityResultContracts.RequestPermission()) { hasPermission ->
            if (!hasPermission) return@registerForActivityResult
            try {
                val file = FileHelper.createTempImageFile(requireContext())
                actionTakePhoto.launch(file)
            } catch (e: IOException) {
                e.printStackTrace()
                viewModel.onPhotoError(getString(R.string.photo_file_error))
            }
        }

    private val actionTakePhoto =
        registerForActivityResult(TakePicture()) { imageByteArray ->
            if (imageByteArray != null)
                viewModel.createPhoto(imageByteArray)
            else
                viewModel.onPhotoError(getString(R.string.photo_not_taken_error))
        }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding = FragmentGalleryBinding.bind(view)
        setupRecyclerView()
        setupPhotoButton()

        viewModel.photosFlow.onEach { photos ->
            photoAdapter.submitList(photos)
        }.launchIn(viewLifecycleOwner.lifecycleScope)

        viewModel.events
            .onEach(::setEvent)
            .launchIn(viewLifecycleOwner.lifecycleScope)
    }

    private fun setEvent(event: GalleryUiEvents) = when (event) {
        is GalleryUiEvents.TakePhoto -> actionCameraPermission.launch(Manifest.permission.CAMERA)
        is GalleryUiEvents.Error -> showSnackbar(event.message)
    }

    private fun setupRecyclerView() = with(binding.recyclerView) {
        layoutManager = GridLayoutManager(
            requireContext(),
            resources.getInteger(R.integer.grid_size)
        )
        adapter = photoAdapter
    }

    private fun setupPhotoButton() {
        binding.takePhoto.setOnClickListener {
            viewModel.takePhoto()
        }
    }

    private fun showSnackbar(@StringRes resId: Int) {
        showSnackbar(getString(resId))
    }

    private fun showSnackbar(message: String) {
        Snackbar.make(binding.root, message, Snackbar.LENGTH_SHORT)
    }
}