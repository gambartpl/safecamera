package com.gambart.safecamera.ui.gallery

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.gambart.safecamera.copyAndInsert
import com.gambart.safecamera.model.Photo
import com.gambart.safecamera.replaceIf
import com.gambart.safecamera.repository.PhotoRepository
import kotlinx.coroutines.flow.*
import kotlinx.coroutines.launch

sealed class GalleryUiEvents {
    object TakePhoto: GalleryUiEvents()
    data class Error(val message: String): GalleryUiEvents()
}

/**
 * Has one state only and therefore photos flow is exposed directly.
 */
class GalleryViewModel(private val photoRepository: PhotoRepository) : ViewModel() {

    private val _photosFlow = MutableStateFlow<List<Photo>>(emptyList())
    val photosFlow: StateFlow<List<Photo>> = _photosFlow.asStateFlow()

    private val _events = MutableSharedFlow<GalleryUiEvents>()
    val events = _events.asSharedFlow()

    init {
        observePhotos()
        retrievePhotos()
    }

    private fun observePhotos() {
        photoRepository.processedPhotosFlow.onEach { photo ->
            _photosFlow.value = _photosFlow.value.replaceIf(photo) {
                it.fileName == photo.fileName
            }
        }.launchIn(viewModelScope)
    }

    private fun retrievePhotos() = viewModelScope.launch {
        _photosFlow.value = photoRepository.getPhotos()
    }

    fun createPhoto(imageByteArray: ByteArray) = viewModelScope.launch {
        val photo = photoRepository.createPhoto(imageByteArray)
        _photosFlow.value = _photosFlow.value.copyAndInsert(photo)
    }

    fun takePhoto() = viewModelScope.launch {
        _events.emit(GalleryUiEvents.TakePhoto)
    }

    fun onPhotoError(message: String) = viewModelScope.launch {
        _events.emit(GalleryUiEvents.Error(message))
    }
}