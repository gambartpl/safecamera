package com.gambart.safecamera.ui.login

import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import com.gambart.safecamera.R
import com.gambart.safecamera.databinding.FragmentLoginBinding
import com.gambart.safecamera.utils.autoCleared
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach
import org.koin.androidx.viewmodel.ext.android.viewModel

class LoginFragment: Fragment(R.layout.fragment_login) {

    private val viewModel by viewModel<LoginViewModel>()
    private var binding by autoCleared<FragmentLoginBinding>()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding = FragmentLoginBinding.bind(view)
        binding.loginButton.setOnClickListener {
            viewModel.login(binding.loginEditText.text.toString())
        }

        viewModel.state.onEach(::setState).launchIn(viewLifecycleOwner.lifecycleScope)
    }

    private fun setState(state: LoginViewState) = when (state) {
        LoginViewState.LoginSuccess -> navigateToPhotoScreen()
        LoginViewState.LoginError -> showLoginError()
        else -> { }
    }

    private fun showLoginError() {
        binding.loginInput.error = getString(R.string.login_error)
    }

    private fun navigateToPhotoScreen() {
        findNavController().navigate(LoginFragmentDirections.actionLoginFragmentToGalleryFragment())
    }
}