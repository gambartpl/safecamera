package com.gambart.safecamera.ui.login

import androidx.lifecycle.ViewModel
import com.gambart.safecamera.repository.UserRepository
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow

sealed class LoginViewState {
    object Default : LoginViewState()
    object LoginSuccess : LoginViewState()
    object LoginError : LoginViewState()
}

class LoginViewModel(
    private val userRepository: UserRepository
) : ViewModel() {

    private val _state = MutableStateFlow<LoginViewState>(LoginViewState.Default)
    val state: StateFlow<LoginViewState> = _state

    fun login(password: String) {
        _state.value =
            if (userRepository.isPasswordValid(password)) LoginViewState.LoginSuccess
            else LoginViewState.LoginError
    }
}