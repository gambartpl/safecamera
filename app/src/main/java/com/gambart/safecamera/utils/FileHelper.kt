package com.gambart.safecamera.utils

import android.content.Context
import android.os.Environment
import java.io.File
import java.io.IOException
import java.text.SimpleDateFormat
import java.time.ZoneOffset
import java.time.format.DateTimeFormatter
import java.util.*

val tempFileDateFormat = SimpleDateFormat("yyyyMMdd_", Locale.getDefault())
val fileDateFormat = SimpleDateFormat("yyyyMMdd_HHmmssSSS", Locale.getDefault())
val shortDateFormat = SimpleDateFormat("yyyy-MM-dd HH:mm", Locale.getDefault())

object FileHelper {

    fun getEncryptedFileName() = "${fileDateFormat.format(Date())}"

    @Throws(IOException::class)
    fun createTempImageFile(context: Context): File {
        val timestamp: String = tempFileDateFormat.format(Date())
        val directory: File? = context.getExternalFilesDir(Environment.DIRECTORY_PICTURES)
        return File.createTempFile(timestamp, ".jpg", directory)
    }

    fun createImageFile(name: String, context: Context): File =
        File(context.getExternalFilesDir(Environment.DIRECTORY_PICTURES), name)

    fun getFiles(context: Context): List<File> {
        val directory: File? = context.getExternalFilesDir(Environment.DIRECTORY_PICTURES)
        val files = directory?.listFiles() ?: emptyArray()
        return files.toList()
    }
}